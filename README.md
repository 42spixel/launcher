# **Launcher**

Nginx server for coming soon page.

### **Credits**

+ [nginx](https://hub.docker.com/_/nginx/)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/launcher/src/master/LICENSE.md)**.
